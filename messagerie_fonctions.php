<?php
/*
 * Plugin messagerie / gestion des messages
 * Licence GPL
 * (c) depuis 2008 Collectif SPIP
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Determiner le nombre de nouveaux messages non lus par l'auteur
 *
 * @param int $id_auteur
 * @return int ou string vide
 */
function messagerie_messages_non_lus($id_auteur) {
	static $messages = [];
	if (!isset($messages[$id_auteur])) {
		include_spip('base/abstract_sql');
		$messages[$id_auteur] = sql_countsel('spip_auteurs_messages', ['id_auteur=' . intval($id_auteur), "vu<>'oui' AND vu<>'poub'"]);
		if (!$messages[$id_auteur]) {
			$messages[$id_auteur] = '';
		}
	}
	return $messages[$id_auteur];
}

/**
 * Afficher un texte 'Vous avez un nouveau message' ou 'Vous avez N nouveaux messages'
 * en fonction du nombre de messages
 *
 * @param int ou string vide
 * @return string
 */
function messagerie_texte_nouveaux_messages($nombre) {

	if (!function_exists('singulier_ou_pluriel')) {
		include_spip('inc/filtres');
	}
	return singulier_ou_pluriel(intval($nombre), 'messagerie:texte_un_nouveau_message', 'messagerie:texte_des_nouveaux_messages');
}
