# Messagerie v4


## Installer le plugin.


Créer un article, et ajouter dedans :
```
{{{Envoyer un message}}}
<formulaire|ecrire_message>
```

Creer un second article, et ajouter dedans :
```
{{{Messages recus}}}
<formulaire|messages_recus>
```

Creer un troisieme article, et ajouter dedans :
```
{{{Messages envoyes}}}
<formulaire|messages_envoyes>
```


## Options


### `_REDIRECT_POST_ENVOI_MESSAGE`

Cette constante permets de definir l'url de la page de redirection apres envoi d'un message - par exemple pour aller directement sur la liste des messages envoyés.

Elle peut contenir une URL complète ou une url raccourcie SPIP :

```
define('_REDIRECT_POST_ENVOI_MESSAGE','art123');
```


### `_URL_ENVOYER_MESSAGE`

Cette constante permet de définir l'URL de la page pour envoyer un message qui sera utilisee pour les bulles "répondre".

Elle peut contenir une URL complète ou une url raccourcie SPIP :

```
define('_URL_ENVOYER_MESSAGE','art123');
```


### `_NB_MESSAGES_MAX_JOUR`

La constante `_NB_MESSAGES_MAX_JOUR` permet de définir un nombre maximum de message par jour pour un expéditeur.
Limite les risques d'usage abusifs

```
define('_NB_MESSAGES_MAX_JOUR', 25);
```


## Personalisations


2 Pipelines permettent de personaliser le fonctionnement du plugin

### `messagerie_destiner`

Permet d'ajouter/filtrer les destinataires en fonction des droits du visiteur

### `messagerie_signer_message`

Permets d'ajouter une signature automatique en fonction de la personne connecté.
