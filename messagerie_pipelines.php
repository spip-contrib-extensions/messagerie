<?php
/*
 * Plugin messagerie / gestion des messages
 * Licence GPL
 * (c) depuis 2008 Collectif SPIP
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Pipeline messagerie_signer_message
 * Ajout d'une signature en bas de mail
 *
 * @param string $texte
 * @return string
 */
function messagerie_messagerie_signer_message($texte) {
	$texte .= _T('messagerie:texte_signature_email', ['nom_site' => $GLOBALS['meta']['nom_site'], 'url_site' => $GLOBALS['meta']['adresse_site']]);
	return $texte;
}

/**
 * Déclarer le formulaires à nospam
 * @param $formulaires
 * @return mixed
 */
function messagerie_nospam_lister_formulaires($formulaires) {
	$formulaires[] = 'ecrire_message';

	return $formulaires;
}

/**
 * Pipeline inserthead.
 * Ajout d'une css dans l'espace public
 *
 * @param string $texte
 * @return string
 */
function messagerie_insert_head($texte) {
	$texte .= '<link rel="stylesheet" type="text/css" href="' . find_in_path('habillage/messagerie.css') . '" media="all" />' . "\n";
	return $texte;
}


function messagerie_jquery_plugins($scripts) {
	$scripts[] = 'javascript/jquery.autocompleter.js';
	return $scripts;
}

function messagerie_messagerie_statuts_destinataires_possibles() {
	include_spip('inc/filtres_ecrire');
	return auteurs_lister_statuts('tous', false);
}
