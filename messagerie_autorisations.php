<?php
/*
 * Plugin messagerie / gestion des messages
 * Licence GPL
 * (c) depuis 2008 Collectif SPIP
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

function messagerie_autoriser() {
}


/**
 * Vérifier l'autorisation à répondre à un message
 *
 */
function autoriser_message_repondre($faire, $type = '', $id = 0, $qui = null, $opt = null) {
	return true;
}


/**
 * Verifier l'autorisation de choisir le destinataire d'un message
 *
 */
function autoriser_message_destiner_dist($faire, $quoi, $id, $qui, $opts) {
	// par defaut, le champ destinataire est toujours active
	return true;
}

/**
 * Verifier l'autorisation d'envoyer un message a tout le monde.
 * en indiquant 'general' comme destinataire
 * peut etre redefini par
 * define('_EMAIL_GENERAL','general@domaine.org');
 * dans mes_options.php
 *
 */
function autoriser_message_destiner_general_dist($faire, $quoi, $id, $qui, $opts) {
	// par defaut, seuls les admins ont le droit de spammer
	return $qui['statut'] == '0minirezo';
}
