<?php
/*
 * Plugin messagerie / gestion des messages
 * Licence GPL
 * (c) depuis 2008 Collectif SPIP
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Signaler la lecture d'un message (marque comme lu)
 *
 */
function action_liremessage_dist() {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$id_message = $securiser_action();
	
	include_spip('inc/messages');
	messagerie_marquer_lus($GLOBALS['visiteur_session']['id_auteur'], [intval($id_message)]);
}
