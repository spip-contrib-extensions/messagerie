function set_auteur(li){
	var id_auteur;
	var $dest = jQuery("#destinataire");
	var box = $dest.siblings('.details');
	if (li!==undefined && li.extra[0]) {
		id_auteur = li.extra[0];
		var nom = jQuery(li).html();
		if (box.find('input[value='+id_auteur+']').length === 0){
			box.append(" <span class='dest'>"
			+ nom
			+ "<input type='hidden' name='destinataires[]' value='"+id_auteur+"' /> "
			+ box.find('span.dest:first').html()
			+ "</span>");
		}
	}
	box
	  .find('span.dest')
	  .hover(function(){jQuery(this).addClass('hover');},function(){jQuery(this).removeClass('hover');})
	  .find('img').click(function(){jQuery(this).parent().remove();});
	$dest.attr('value','');//.get(0).focus();
}
function formulaire_ecrire_message_init(){
	var $dest = jQuery("#destinataire");
	if ($dest.length) {
		if ($dest[0].autocompleter === undefined) {
			$dest.autocomplete(url_find_friend, {minChars:3, mustMatchOrEmpty:1,selectFirst:true,matchSubset:0, matchContains:1, cacheLength:10, onItemSelect:set_auteur });
			$dest.parent().bind('click',function(){jQuery('#destinataire').get(0).focus();});
			set_auteur();
		}
	}
}
if (window.jQuery){
	jQuery(function(){
		formulaire_ecrire_message_init();
		onAjaxLoad(formulaire_ecrire_message_init);
	});
}
